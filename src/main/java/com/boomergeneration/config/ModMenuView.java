package com.boomergeneration.config;

import com.boomergeneration.config.data.BGConfig;
import com.boomergeneration.config.gui.BoomerScreen;

import com.terraformersmc.modmenu.api.ConfigScreenFactory;
import com.terraformersmc.modmenu.api.ModMenuApi;

public class ModMenuView implements ModMenuApi
{
    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory()
    {
        return (screen) -> {
            BoomerScreen gui = new BoomerScreen( new BGConfig(), screen);
            return gui;
        };
    }
}