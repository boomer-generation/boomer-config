package com.boomergeneration.config.gui.widgets;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.narration.NarrationElementOutput;

public class BoomerButton extends BoomerAbstractButton {
    protected final BoomerButton.OnPress onPress;

    public BoomerButton(int x, int y, int width, int height, int color, String text, OnPress onPress) {
        super(x, y, width, height, color, text);
        this.onPress = onPress;
    }

    @Override
    public void onPress() {
        this.onPress.onPress(this);
    }

    @Override
    public NarrationPriority narrationPriority() {
        return NarrationPriority.NONE;
    }

    @Override
    public void updateNarration(NarrationElementOutput narrationElementOutput) {

    }

    @Environment(EnvType.CLIENT)
    public interface OnPress {
        void onPress(BoomerButton button);
    }
}
