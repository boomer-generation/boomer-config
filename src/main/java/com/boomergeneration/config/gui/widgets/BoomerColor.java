package com.boomergeneration.config.gui.widgets;
public class BoomerColor {
    public static float alpha;
    public static float red;
    public static float green;
    public static float blue;
    public static BoomerColor fromInt(int color) {
        BoomerColor ret = new BoomerColor();
        ret.alpha = (float) (color >> 24 & 255) / 255.0F;
        ret.red = (float) (color >> 16 & 255) / 255.0F;
        ret.green = (float) (color >> 8 & 255) / 255.0F;
        ret.blue = (float) (color & 255) / 255.0F;
        return ret;
    }
}
