package com.boomergeneration.config.gui.widgets;

import com.mojang.blaze3d.vertex.PoseStack;
import net.fabricmc.example.ExampleMod;
import net.minecraft.client.gui.components.Widget;
import net.minecraft.client.gui.narration.NarrationElementOutput;

public class BoomerRectangle extends BoomerWidget implements Widget {

    private static int color = 0;
    public BoomerRectangle(int color) {
        this.color = color;
    }

    @Override
    public void render(PoseStack poseStack, int i, int j, float f){
        if(this.visible){
            this.drawRect(this.x, this.y, this.width, this.height, this.color);
        }
    }


    // we don't care about the mouse area in a rectangle. Its a rectangle and not a button.
    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        return false;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public void updateNarration(NarrationElementOutput narrationElementOutput) {

    }

    @Override
    public NarrationPriority narrationPriority() {
        return NarrationPriority.NONE;
    }

    public void drawBorder(int color, int borderWidth) {
        BoomerColor c = BoomerColor.fromInt(color);
        this.buffQuads(vertices -> {
            addQuad(vertices, this.x, this.y, this.width, this.y + borderWidth, c.alpha, c.red, c.green, c.blue);
            addQuad(vertices, this.x, this.height - borderWidth, this.width, this.height, c.alpha, c.red, c.green, c.blue);
            addQuad(vertices, this.x, this.y, this.x + borderWidth, this.height, c.alpha, c.red, c.green, c.blue);
            addQuad(vertices, this.width - borderWidth, this.y, this.width, this.height, c.alpha, c.red, c.green, c.blue);
        });
    }
}
