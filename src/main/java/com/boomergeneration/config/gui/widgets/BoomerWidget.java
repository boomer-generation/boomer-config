package com.boomergeneration.config.gui.widgets;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.*;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.client.gui.components.Widget;
import net.minecraft.client.gui.components.events.GuiEventListener;
import net.minecraft.client.gui.narration.NarratableEntry;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.resources.sounds.SimpleSoundInstance;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvents;

import java.util.function.Consumer;

public abstract class BoomerWidget extends GuiComponent implements Widget, GuiEventListener, NarratableEntry {
    protected final Font font;
    public boolean visible = true;
    public int x = 0;
    public int y = 0;
    public int z = 0;
    public int width = 128;
    public int height = 72;

    // Gui object that is in Keyboard focus or 1st in the load
    public boolean focus = false;

    public double opacity = 1.0;

    protected BoomerWidget() {
        this.font = Minecraft.getInstance().font;
    }

    //////////////
    // Drawing
    //////////////

    protected void drawString(PoseStack matrixStack, String str, int x, int y, int color) {
        this.font.draw(matrixStack, str, x, y, color);
    }

    protected void drawString(PoseStack matrixStack, Component text, int x, int y, int color) {
        this.font.draw(matrixStack, text, x, y, color);
    }

    protected void drawRect(double x1, double y1, double x2, double y2, int color) {
        BoomerColor cH = BoomerColor.fromInt(color);
        this.buffQuads(vertices -> addQuad(vertices, x1, y1, x2, y2, cH.alpha, cH.red, cH.green, cH.blue));
    }

    protected void drawTriangle(double x1, double y1, double x2, int color) {
        BoomerColor cH = BoomerColor.fromInt(color);
        this.bufTriangles(vertices -> addLine(vertices, x1, y1, x2, cH.alpha, cH.red, cH.green, cH.blue));
    }

    protected void buffQuads(Consumer<VertexConsumer> consumer) {
        RenderSystem.setShader(GameRenderer::getPositionColorShader);
        RenderSystem.enableBlend();
        RenderSystem.disableTexture();
        RenderSystem.defaultBlendFunc();
        BufferBuilder bufferBuilder = Tesselator.getInstance().getBuilder();
        bufferBuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_COLOR);

        consumer.accept(bufferBuilder);

        BufferBuilder.RenderedBuffer output = bufferBuilder.end();

        BufferUploader.drawWithShader( output );
        RenderSystem.enableTexture();
        RenderSystem.disableBlend();
    }

    protected void bufTriangles(Consumer<VertexConsumer> consumer) {
        RenderSystem.setShader(GameRenderer::getPositionColorShader);
        RenderSystem.enableBlend();
        RenderSystem.disableTexture();
        RenderSystem.defaultBlendFunc();
        BufferBuilder bufferBuilder = Tesselator.getInstance().getBuilder();
        bufferBuilder.begin(VertexFormat.Mode.TRIANGLES, DefaultVertexFormat.POSITION_COLOR);

        consumer.accept(bufferBuilder);

        BufferBuilder.RenderedBuffer output = bufferBuilder.end();

        BufferUploader.drawWithShader( output );
        RenderSystem.enableTexture();
        RenderSystem.disableBlend();
    }

    protected static void addQuad(VertexConsumer consumer, double x1, double y1, double x2, double y2, float a, float r, float g, float b) {
        consumer.vertex(x2, y1, 0.0D).color(r, g, b, a).endVertex();
        consumer.vertex(x1, y1, 0.0D).color(r, g, b, a).endVertex();
        consumer.vertex(x1, y2, 0.0D).color(r, g, b, a).endVertex();
        consumer.vertex(x2, y2, 0.0D).color(r, g, b, a).endVertex();
    }

    protected static void addLine(VertexConsumer consumer, double x1, double y1, double x2, float a, float r, float g, float b) {
        consumer.vertex(x2, y1, 0.0D).color(r, g, b, a).endVertex();
        consumer.vertex(x1, y1, 0.0D).color(r, g, b, a).endVertex();
        consumer.vertex(x2, x1, 0.0D).color(r, g, b, a).endVertex();
    }

    protected void playClickSound() {
        Minecraft.getInstance().getSoundManager()
                .play(SimpleSoundInstance.forUI(SoundEvents.UI_BUTTON_CLICK, 1.0F));
    }

    protected int getStringWidth(String text) {
        return this.font.width(text);
    }

    public NarratableEntry.NarrationPriority getType() {
         return NarratableEntry.NarrationPriority.NONE;
    }


    public void onClick(double d, double e) {
    }

    public void onRelease(double d, double e) {
    }

    protected void onDrag(double d, double e, double f, double g) {
    }

    public boolean mouseClicked(double d, double e, int i) {
        boolean ret = false;
        if ( this.visible && i == 0 && this.clicked(d, e) ) {
            this.playClickSound();
            this.onClick(d, e);
            ret = true;
        }
        return ret;
    }
    protected boolean clicked(double d, double e) {
        return this.visible &&
                d >= (double)this.x &&
                e >= (double)this.y &&
                d < (double)(this.x + this.width) &&
                e < (double)(this.y + this.height);
    }
    public boolean mouseReleased(double d, double e, int i) {
        boolean ret = false;
        if (i == 0) {
            this.onRelease(d, e);
            ret = true;
        }
        return ret;
    }
}
