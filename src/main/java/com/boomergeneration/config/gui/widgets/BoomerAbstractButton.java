package com.boomergeneration.config.gui.widgets;

import com.mojang.blaze3d.vertex.PoseStack;
import net.fabricmc.example.ExampleMod;
import net.minecraft.client.gui.components.Widget;
import net.minecraft.network.chat.Component;


public abstract class BoomerAbstractButton extends BoomerWidget implements Widget {
    private static Component text = Component.translatable("");
    private static int color = 0XFF000000;
    public abstract void onPress();

    public BoomerAbstractButton(int x, int y, int width, int height, int color, String text) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.text = Component.translatable(text);
        this.color = color;
    }

    public void onClick(double d, double e) {
        this.onPress();
    }
    @Override
    public void render(PoseStack poseStack, int i, int j, float f){
        if(this.visible){
            this.drawRect(this.x, this.y, this.width, this.height, this.color);
            int strWidth = this.font.width(text);
            int textHeight = Math.round( 9 / 2 );
            int centerX = this.x  + (this.width/2);
            int centerY = this.y  + textHeight;
            this.drawString(poseStack, this.text, centerX - (strWidth / 2), centerY -  - 4, 0XFFFFFFFF);
        }
    }

    protected boolean clicked(double d, double e) {
        return this.visible &&
                d >= (double)this.x &&
                e >= (double)this.y &&
                d < (double)(this.x + this.width) &&
                e < (double)(this.y + this.height);
    }

    @Override
    public boolean mouseClicked(double d, double e, int i) {
        super.mouseClicked(d, e, i);
        ExampleMod.LOGGER.info(
                this.text.toString() +
                "MOUSE X: " + d +
                " MOUSE Y: " +  e +
                " BUTTON " + i );

        this.playClickSound();
        return true;
    }
}
