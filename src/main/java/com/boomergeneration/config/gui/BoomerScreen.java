package com.boomergeneration.config.gui;

import com.boomergeneration.config.data.BGConfig;
import com.boomergeneration.config.gui.widgets.BoomerButton;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.*;
import it.unimi.dsi.fastutil.booleans.BooleanConsumer;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.resources.ResourceLocation;


public class BoomerScreen extends Screen {
    private final Screen parent;
    private final BGConfig CONFIG;

    private int cIndex;

    // just for testing
    public static final ResourceLocation IMAGE_BACKGROUND_LOCATION = new ResourceLocation("textures/gui/options_background.png");


    protected final BooleanConsumer callback = new BooleanConsumer() {
        @Override
        public void accept(boolean b) {
            if( b ){
                onClose();
            }else{
                // not sure
            }
        }
    };


    public BoomerScreen(BGConfig config, Screen parent) {
        super(config.getTitle());
        this.CONFIG = config;
        this.parent = parent;
        this.cIndex = 0;
    }

    @Override
    protected void init() {
        super.init();
    }

    @Override
    public void render(PoseStack poseStack, int i, int j, float f) {
        // draw image backgound (dirt)
        this.renderImageBackground(0, IMAGE_BACKGROUND_LOCATION);
        // draw gradient over the background
        // this.fillGradient(poseStack, 0, 0, width, height, -1072689136, -804253680);

        this.addRenderableWidget(
                new BoomerButton(20, 20, 128,72, 0XFFFFFF00,"btn1", (button) -> {
                    this.callback.accept(true);
                })
        );


        this.addRenderableWidget(
                new BoomerButton(100, 100, 128,72, 0XFF00FF00,"btn2", (button) -> {
                    this.callback.accept(false);
                })
        );

        super.render(poseStack, i, j, f);
    }

    @Override
    public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
        return super.keyPressed(keyCode, scanCode, modifiers);
    }

    @Override
    public void tick() {
        super.tick();
    }

    @Override
    public boolean shouldCloseOnEsc() {
        return true;
    }

    @Override
    public void onClose() {
        this.minecraft.setScreen(null);
    }

    public void renderImageBackground(int i, ResourceLocation imageLoc) {
        Tesselator tesselator = Tesselator.getInstance();
        BufferBuilder bufferBuilder = tesselator.getBuilder();
        RenderSystem.setShader(GameRenderer::getPositionTexColorShader);
        RenderSystem.setShaderTexture(0, imageLoc);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        float f = 32.0F;
        bufferBuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX_COLOR);
        bufferBuilder.vertex(0.0, (double)this.height, 0.0).uv(0.0F, (float)this.height / 32.0F + (float)i).color(64, 64, 64, 255).endVertex();
        bufferBuilder.vertex((double)this.width, (double)this.height, 0.0).uv((float)this.width / 32.0F, (float)this.height / 32.0F + (float)i).color(64, 64, 64, 255).endVertex();
        bufferBuilder.vertex((double)this.width, 0.0, 0.0).uv((float)this.width / 32.0F, (float)i).color(64, 64, 64, 255).endVertex();
        bufferBuilder.vertex(0.0, 0.0, 0.0).uv(0.0F, (float)i).color(64, 64, 64, 255).endVertex();
        tesselator.end();
    }
}